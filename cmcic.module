<?php
/**
 * Implements hook_menu().
 */
function cmcic_menu() {
  $items = array();

  // Define an always accessible path to receive IPNs.
  $items['commerce/payment/cmcic/ipn'] = array(
    'page callback' => 'cmcic_process_ipn',
    'page arguments' => array(),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}
/**
 * Implements hook_commercd_payment_method_info()
 */
function cmcic_commerce_payment_method_info() {
  $payment_methods['cmcic'] = array(
  	'base' => 'cmcic',
    'title' => t(' Crédit Mutuel - CyberMut'), 
    'short_title' => t('Crédit Mutuel - CyberMut'), 
    'description' => t('Plateforme de Paiement Crédit Mutuel - CyberMut'), 
    'terminal' => FALSE, 
    'offsite' => TRUE, 
    'offsite_autoredirect' => TRUE,
    'active' => true,
    'callback' => array(
		'settings_form' => 'cmcic_settings_form',
	),
  );
  return $payment_methods;
}

/**
 * Fonction de génération du formulaire de paramétrage du paiement
 */
 function cmcic_settings_form($settings = array()){
	if(count($settings) == 0){
		$settings = array(
			'code_scte' => '',
			'code_tpe' => '',
			'cle' => '',
			'environnement' => 0,
		);
	}
	$form = array();
 	$form['code_scte'] = array(
		'#type' => 'textfield',
		'#title' => t('Code société'),
		'#default_value' => $settings['code_scte'],
	);
 	$form['code_tpe'] = array(
		'#type' => 'textfield',
		'#title' => t('Code TPE'),
		'#default_value' => $settings['code_tpe'],
	);
 	$form['cle'] = array(
		'#type' => 'textfield',
		'#title' => t('Clé privée'),
		'#default_value' => $settings['cle'],
	);
 	$form['environnement'] = array(
		'#type' => 'select',
		'#title' => t('Environnement'),
		'#options' => array(
			0 => t('Test'),
			1 => t('Production'),
		),
		'#default_value' => $settings['environnement'],
	);
	return $form;
 }


/**
 * Fonction de génération du formulaire d'envoi de la requête au service CM CIC
 */
 function cmcic_redirect_form($form, &$form_state, $order, $payment_method){
	 	/*****************************************************************************
	 *
	 * "Open source" kit for CM-CIC P@iement (TM)
	 *
	 * File "Phase1Aller.php":
	 *
	 * Author   : Euro-Information/e-Commerce (contact: centrecom@e-i.com)
	 * Version  : 1.04
	 * Date     : 01/01/2009
	 *
	 * Copyright: (c) 2009 Euro-Information. All rights reserved.
	 * License  : see attached document "License.txt".
	 *
	 *****************************************************************************/
	
	// TPE Settings
	// Warning !! CMCIC_Config contains the key, you have to protect this file with all the mechanism available in your development environment.
	// You may for instance put this file in another directory and/or change its name. If so, don't forget to adapt the include path below.
	
	// PHP implementation of RFC2104 hmac sha1 ---
	require_once(drupal_get_path('module', 'cmcic')."/api/CMCIC_Config.php");
	require_once(drupal_get_path('module', 'cmcic')."/api/CMCIC_Tpe.inc.php");
	
	$sOptions = "";
	
	// ----------------------------------------------------------------------------
	//  CheckOut Stub setting fictious Merchant and Order datas.
	//  That's your job to set actual order fields. Here is a stub.
	// -----------------------------------------------------------------------------
	
	// Reference: unique, alphaNum (A-Z a-z 0-9), 12 characters max
	$sReference = $order->order_number;
	
	// Amount : format  "xxxxx.yy" (no spaces)
	$sMontant = $order->commerce_order_total['und'][0]['amount'] / 100;
	
	// Currency : ISO 4217 compliant
	$sDevise  = $order->commerce_order_total['und'][0]['currency_code'];
	
	// free texte : a bigger reference, session context for the return on the merchant website
	$sTexteLibre = "";
	
	// transaction date : format d/m/y:h:m:s
	$sDate = date("d/m/Y:H:i:s");
	
	// Language of the company code
	$sLangue = "FR";
	
	// customer email
	$sEmail = $order->mail;
	
	// ----------------------------------------------------------------------------
	
	// between 2 and 4
	//$sNbrEch = "4";
	$sNbrEch = "";
	
	// date echeance 1 - format dd/mm/yyyy
	//$sDateEcheance1 = date("d/m/Y");
	$sDateEcheance1 = "";
	
	// montant échéance 1 - format  "xxxxx.yy" (no spaces)
	//$sMontantEcheance1 = "0.26" . $sDevise;
	$sMontantEcheance1 = "";
	
	// date echeance 2 - format dd/mm/yyyy
	$sDateEcheance2 = "";
	
	// montant échéance 2 - format  "xxxxx.yy" (no spaces)
	//$sMontantEcheance2 = "0.25" . $sDevise;
	$sMontantEcheance2 = "";
	
	// date echeance 3 - format dd/mm/yyyy
	$sDateEcheance3 = "";
	
	// montant échéance 3 - format  "xxxxx.yy" (no spaces)
	//$sMontantEcheance3 = "0.25" . $sDevise;
	$sMontantEcheance3 = "";
	
	// date echeance 4 - format dd/mm/yyyy
	$sDateEcheance4 = "";
	
	// montant échéance 4 - format  "xxxxx.yy" (no spaces)
	//$sMontantEcheance4 = "0.25" . $sDevise;
	$sMontantEcheance4 = "";
	
	// ----------------------------------------------------------------------------
	
	$oTpe = new CMCIC_Tpe($sLangue);     		
	$oHmac = new CMCIC_Hmac($oTpe);      	        
	
	// Control String for support
	$CtlHmac = sprintf(CMCIC_CTLHMAC, $oTpe->sVersion, $oTpe->sNumero, $oHmac->computeHmac(sprintf(CMCIC_CTLHMACSTR, $oTpe->sVersion, $oTpe->sNumero)));
	
	// Data to certify
	$PHP1_FIELDS = sprintf(CMCIC_CGI1_FIELDS,     $oTpe->sNumero,
	                                              $sDate,
	                                              $sMontant,
	                                              $sDevise,
	                                              $sReference,
	                                              $sTexteLibre,
	                                              $oTpe->sVersion,
	                                              $oTpe->sLangue,
	                                              $oTpe->sCodeSociete, 
	                                              $sEmail,
	                                              $sNbrEch,
	                                              $sDateEcheance1,
	                                              $sMontantEcheance1,
	                                              $sDateEcheance2,
	                                              $sMontantEcheance2,
	                                              $sDateEcheance3,
	                                              $sMontantEcheance3,
	                                              $sDateEcheance4,
	                                              $sMontantEcheance4,
	                                              $sOptions);
	
	// MAC computation
	$sMAC = $oHmac->computeHmac($PHP1_FIELDS);
	
	$form = array();
	$form['#action'] = CMCIC_SERVEUR;
	
	$form['version'] = array('#type' => 'hidden', '#default_value' => $oTpe->sVersion);
	$form['societe'] = array('#type' => 'hidden', '#default_value' => $oTpe->sCodeSociete);
	$form['TPE'] = array('#type' => 'hidden', '#default_value' => $oTpe->sNumero);
	$form['date'] = array('#type' => 'hidden', '#default_value' => $sDate);
	$form['montant'] = array('#type' => 'hidden', '#default_value' => $sMontant . $sDevise);
	$form['reference'] = array('#type' => 'hidden', '#default_value' => $sReference);
	$form['MAC'] = array('#type' => 'hidden', '#default_value' => $sMAC);
	$form['url_retour'] = array('#type' => 'hidden', '#default_value' => $oTpe->sUrlKO);
	$form['url_retour_ok'] = array('#type' => 'hidden', '#default_value' => $oTpe->sUrlOK);
	$form['url_retour_err'] = array('#type' => 'hidden', '#default_value' => $oTpe->sUrlKO);
	$form['lgue'] = array('#type' => 'hidden', '#default_value' => $oTpe->sLangue);
	$form['texte-libre'] = array('#type' => 'hidden', '#default_value' => HtmlEncode($sTexteLibre));
	$form['mail'] = array('#type' => 'hidden', '#default_value' => $sEmail);
	$form['mail'] = array('#type' => 'hidden', '#default_value' => $sEmail);
	
	return $form;
}
/**
 * Fonction de réception des IPN
 */
function cmcic_process_ipn(){
	$order = commerce_order_load($_POST['reference']);
	$payment_method = commerce_payment_method_instance_load($order->data['payment_method']);
		
		/*****************************************************************************
	 *
	 * "Open source" kit for CM-CIC P@iement(TM).
	 * Process CMCIC Payment. Sample RFC2104 compliant with PHP4 skeleton.
	 *
	 * File "Phase2Retour.php":
	 *
	 * Author   : Euro-Information/e-Commerce (contact: centrecom@e-i.com)
	 * Version  : 1.04
	 * Date     : 01/01/2009
	 *
	 * Copyright: (c) 2009 Euro-Information. All rights reserved.
	 * License  : see attached document "Licence.txt".
	 *
	 *****************************************************************************/
	
	header("Pragma: no-cache");
	header("Content-type: text/plain");
	
	// TPE Settings
	// Warning !! CMCIC_Config contains the key, you have to protect this file with all the mechanism available in your development environment.
	// You may for instance put this file in another directory and/or change its name. If so, don't forget to adapt the include path below.
	
	// --- PHP implementation of RFC2104 hmac sha1 ---
	require_once(drupal_get_path('module', 'cmcic')."/api/CMCIC_Config.php");
	require_once(drupal_get_path('module', 'cmcic')."/api/CMCIC_Tpe.inc.php");
	
	
	// Begin Main : Retrieve Variables posted by CMCIC Payment Server 
	$CMCIC_bruteVars = getMethode();
	
	// TPE init variables
	$oTpe = new CMCIC_Tpe();
	$oHmac = new CMCIC_Hmac($oTpe);
	
	// Message Authentication
	$cgi2_fields = sprintf(CMCIC_CGI2_FIELDS, $oTpe->sNumero,
						  $CMCIC_bruteVars["date"],
					      $CMCIC_bruteVars['montant'],
					      $CMCIC_bruteVars['reference'],
					      $CMCIC_bruteVars['texte-libre'],
					      $oTpe->sVersion,
					      $CMCIC_bruteVars['code-retour'],
						  $CMCIC_bruteVars['cvx'],
						  $CMCIC_bruteVars['vld'],
						  $CMCIC_bruteVars['brand'],
						  $CMCIC_bruteVars['status3ds'],
						  $CMCIC_bruteVars['numauto'],
						  $CMCIC_bruteVars['motifrefus'],
						  $CMCIC_bruteVars['originecb'],
						  $CMCIC_bruteVars['bincb'],
						  $CMCIC_bruteVars['hpancb'],
						  $CMCIC_bruteVars['ipclient'],
						  $CMCIC_bruteVars['originetr'],
						  $CMCIC_bruteVars['veres'],
						  $CMCIC_bruteVars['pares']
						);
	
	
	if ($oHmac->computeHmac($cgi2_fields) == strtolower($CMCIC_bruteVars['MAC']))
		{
		switch($CMCIC_bruteVars['code-retour']) {
			case "Annulation" :
				// Payment has been refused
				// put your code here (email sending / Database update)
				// Attention : an autorization may still be delivered for this payment
				commerce_payment_redirect_pane_previous_page($order);
				break;
	
			case "payetest":
				// Payment has been accepeted on the test server
				// put your code here (email sending / Database update)
				commerce_payment_redirect_pane_next_page($order);
				break;
	
			case "paiement":
				// Payment has been accepted on the productive server
				// put your code here (email sending / Database update)
				commerce_payment_redirect_pane_next_page($order);
				break;
	
	
			/*** ONLY FOR MULTIPART PAYMENT ***/
			case "paiement_pf2":
			case "paiement_pf3":
			case "paiement_pf4":
				// Payment has been accepted on the productive server for the part #N
				// return code is like paiement_pf[#N]
				// put your code here (email sending / Database update)
				// You have the amount of the payment part in $CMCIC_bruteVars['montantech']
				break;
	
			case "Annulation_pf2":
			case "Annulation_pf3":
			case "Annulation_pf4":
				// Payment has been refused on the productive server for the part #N
				// return code is like Annulation_pf[#N]
				// put your code here (email sending / Database update)
				// You have the amount of the payment part in $CMCIC_bruteVars['montantech']
				break;
				
		}
	
		$receipt = CMCIC_CGI2_MACOK;
	
	}
	else
	{
		// your code if the HMAC doesn't match
		$receipt = CMCIC_CGI2_MACNOTOK.$cgi2_fields;
	}
	
	//-----------------------------------------------------------------------------
	// Send receipt to CMCIC server
	//-----------------------------------------------------------------------------
	printf (CMCIC_CGI2_RECEIPT, $receipt);
}
