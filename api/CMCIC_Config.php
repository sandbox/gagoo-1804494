<?php
/***************************************************************************************
* Warning !! CMCIC_Config contains the key, you have to protect this file with all     *   
* the mechanism available in your development environment.                             *
* You may for instance put this file in another directory and/or change its name       *
***************************************************************************************/

if(count($payment_method['settings']) < 1){
	drupal_set_message('Vous devez définir les paramètres du module de paiement CM CIC avant d\'effectuer un paiement.');
	header('Location: '.url('admin/commerce/config/payment-methods/manage/commerce_payment_cmcic'));
}

global $base_url;
define("CMCIC_VERSION", "3.0");
define("CMCIC_URLOK", $base_url . '/checkout/' . $order -> order_number . '/payment');
define("CMCIC_URLKO", $base_url . '/checkout/' . $order -> order_number . '/payment');
define("CMCIC_CODESOCIETE", $payment_method['settings']['code_scte']);
define("CMCIC_TPE", $payment_method['settings']['code_tpe']);
define("CMCIC_CLE", $payment_method['settings']['cle']);
if($payment_method['settings']['environnement'] == 0){
	define("CMCIC_SERVEUR", "https://paiement.creditmutuel.fr/test/paiement.cgi");
}
else{
	define("CMCIC_SERVEUR", "https://paiement.creditmutuel.fr/paiement.cgi");
}
?>
